from flask import Flask,request, jsonify,Response
from flask_pymongo import PyMongo
from bson import json_util
from bson.objectid import ObjectId
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.config['MONGO_URI']='mongodb://localhost/finaldent'
mongo = PyMongo(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'



@app.route('/api/finaldent',methods=['POST'])
@cross_origin()
def Crearcitas():
    nombre=request.json['nombre']
    apellido=request.json['apellido']
    edad=request.json['edad']
    telefono=request.json['telefono']
    motivo=request.json['motivo']
    descripcion=request.json['descripcion']
    fecha=request.json['fecha']
    hora=request.json['hora']


    encontfecha = mongo.db.finaldent.find_one({'fecha':fecha})
    
    
    if (encontfecha):
        
        enconthora = mongo.db.finaldent.find_one({'hora':hora})
        
        
        if (enconthora):
            response = jsonify( {'message':'La fecha solicitada de la cita ya esta registrada.'})
            response.status_code = 700
            return response
    
        
        
        
       
    if nombre and apellido and edad and telefono and motivo and descripcion and fecha and hora:
       

        ID = mongo.db.citas.insert(
                
                {
                    'nombre':nombre,
                    'apellido':apellido,
                    'edad':edad,
                    'telefono':telefono,
                    'motivo':motivo,
                    'descripcion':descripcion,
                    'fecha':fecha,
                    'hora':hora,
                }
                
            )
            
        response = {
            'nombre':nombre,
            'apellido':apellido,
            'fecha':fecha,
            'hora':hora
        }
            
        return response
    
    return {'message':'Error'}




@app.route('/api/finaldent',methods=['GET'])
@cross_origin()
def traer_citas():
    
    finaldent = mongo.db.finaldent.find()
    response = json_util.dumps(finaldent)
    return Response(response,mimetype='application/json')


@app.route('/api/finaldent/<ID>',methods=['GET'])
@cross_origin()
def traer_una_cita(ID):


    finaldent =  mongo.db.finaldent.find_one({'_ID':ObjectId(ID)})
 
 
    if (not finaldent):
        response = jsonify( {'message':'La cita no existe dentro de nuesto sistema.'})
        response.status_code = 700
        return response
    response = json_util.dumps(finaldent)
    return Response(response,mimetype="application/json")



@app.route('/api/finaldent/<ID>',methods=['DELETE'])
@cross_origin()
def borrar_una_cita(ID):


    cita =  mongo.db.finaldent.find_one({'_ID':ObjectId(ID)})
    if (not cita):
        response = jsonify( {'message':'La cita no existe dentro de nuestro sistema.'})
        response.status_code = 700
        return response
    
    
    mongo.db.finaldent.delete_one({'_id':ObjectId(ID)})
    return jsonify({'message':'La cita fue eliminada de nuestro sistema exitosamente!!'})


@app.errorhandler(1010)
@cross_origin()
def not_found(error=None):
    response = jsonify( {
        
        'message':'La ruta no fue encontrada!',
        'status':1010
    })
    response.status_code = 1010
    return response

if __name__ == "__main__":
    app.run(debug=True)
    
    
 
    